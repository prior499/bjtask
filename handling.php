<?php

session_start();
include ('db.php');

if(isset($_POST['adminlogin'])) {
    if(empty($_POST['login']) ||  empty($_POST['password'])) {
        setcookie('EmptyAdmin', 1, time()+1);
    } else {
        if($_POST['login'] === 'admin' && $_POST['password'] === '123') {
            $_SESSION['admin_mode'] = 1;
        } else {
            setcookie('ErrAdmin', 1, time()+1);
        }
    }
}
if(isset($_POST['adminexit'])) {
    session_destroy();
}

if(isset($_POST['sortdonea']) || isset($_POST['sortdonez'])) {
    setcookie('TaskSortName', NULL, -1);
    setcookie('TaskSortEmail', NULL, -1);
    if(isset($_POST['sortdonea']))
        setcookie('TaskSortDone', 'A');
    elseif (isset($_POST['sortdonez']))
        setcookie('TaskSortDone', 'Z');
}

if(isset($_POST['sortnamea']) || isset($_POST['sortnamez'])) {
    setcookie('TaskSortDone', NULL, -1);
    setcookie('TaskSortEmail', NULL, -1);
    if(isset($_POST['sortnamea']))
        setcookie('TaskSortName', 'A');
    elseif (isset($_POST['sortnamez']))
        setcookie('TaskSortName', 'Z');
}

if(isset($_POST['sortemaila']) || isset($_POST['sortemailz'])) {
    setcookie('TaskSortDone', NULL, -1);
    setcookie('TaskSortName', NULL, -1);
    if(isset($_POST['sortemaila']))
        setcookie('TaskSortEmail', 'A');
    elseif (isset($_POST['sortemailz']))
        setcookie('TaskSortEmail', 'Z');
}


if(isset($_POST['createtask'])) {
    $name = htmlentities($_POST['Name']);
    $email = $_POST['Email'];
    $text = htmlentities($_POST['Text']);
    setcookie('TaskName', $name, time()+1);
    setcookie('TaskEmail', $email, time()+1);
    setcookie('TaskText', $text, time()+1);
    if(empty($_POST['Name'])) {
        setcookie('ErrName', 1, time()+1);
    } else if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        setcookie('ErrEmail', 1, time()+1);
    } else if(empty($_POST['Text'])) {
        setcookie('ErrText', 1, time()+1);
    } else {
        $sql = "INSERT INTO `task` (`TaskID`, `Edit`, `Name`, `Email`, `Text`) VALUES (NULL, NULL, '$name', '$email', '$text')";
        $pdosql->query($sql);
        setcookie('TaskName', NULL, -1);
        setcookie('TaskEmail', NULL, -1);
        setcookie('TaskText', NULL, -1);
        setcookie('SuccessAddTask', 1, time()+1);
    }

}

if(isset($_POST['undonetask']) && isset($_POST['taskid']) && $_SESSION['admin_mode'] == 1) {
    $taskid = $_POST['taskid'];
    $sql = "UPDATE `task` SET `Done` = 0, `Edit` = '1' WHERE `task`.`TaskID` = '$taskid'";
    $pdosql->query($sql);
}

if(isset($_POST['donetask']) && isset($_POST['taskid'])) {
    if(isset($_SESSION['admin_mode']) && $_SESSION['admin_mode'] == 1) {
        $taskid = $_POST['taskid'];
        $sql = "UPDATE `task` SET `Done` = '1' WHERE `task`.`TaskID` = '$taskid'";
        $pdosql->query($sql);
    } else {
        setcookie('ErrLoginAdmin', 1, time()+1);
    }
}

if(isset($_POST['edittexttask']) && isset($_POST['taskid'])) {
    if(isset($_SESSION['admin_mode']) && $_SESSION['admin_mode'] == 1) {
        $texttask = $_POST['edittexttask'];
        $taskid = $_POST['taskid'];
        $sql = "UPDATE `task` SET `Text` = '$texttask', `Edit` = '1' WHERE `task`.`TaskID` = '$taskid'";
        $pdosql->query($sql);
    } else {
        setcookie('ErrLoginAdmin', 1, time()+1);
    }

}

header('Location: '.$_SERVER['HTTP_REFERER']);


