 <section class="task-list container">
    <table class="table">
        <thead>
        <tr>
            <?php
            $sortdonebutton = 'sortdonea';
            $sortdone = 'Done -';
            if(isset($_COOKIE['TaskSortDone'])) {
                if($_COOKIE['TaskSortDone'] == "A"){
                    $sortdonebutton = 'sortdonez';
                    $sortdone = 'Done &#8595;';
                }
                elseif ($_COOKIE['TaskSortDone'] == "Z") {
                    $sortdonebutton = 'sortdonea';
                    $sortdone = 'Done &#8593;';
                }
            }

            $sortnamebutton = 'sortnamea';
            $sortname = 'Name -';
            if(isset($_COOKIE['TaskSortName'])) {
                if($_COOKIE['TaskSortName'] == "A"){
                    $sortnamebutton = 'sortnamez';
                    $sortname = 'Name &#8595;';
                }
                elseif ($_COOKIE['TaskSortName'] == "Z") {
                    $sortnamebutton = 'sortnamea';
                    $sortname = 'Name &#8593;';
                }
            }

            $sortemailbutton = 'sortemaila';
            $sortemail = 'Email -';
            if(isset($_COOKIE['TaskSortEmail'])) {
                if($_COOKIE['TaskSortEmail'] == "A"){
                    $sortemailbutton = 'sortemailz';
                    $sortemail = 'Email &#8595;';
                }
                elseif ($_COOKIE['TaskSortEmail'] == "Z") {
                    $sortemailbutton = 'sortemaila';
                    $sortemail = 'Email &#8593;';
                }
            }
            ?>
            <th scope="col"></th>
            <th scope="col" class="task-sort">
                <form action="handling.php">
                    <button type="submit" formmethod="post" name="<?php echo $sortdonebutton?>" class="btn btn-light"><?php echo $sortdone?></button>
                </form>
            </th>
            <th scope="col" class="task-sort">
                <form action="handling.php">
                    <button type="submit" formmethod="post" name="<?php echo $sortnamebutton?>" class="btn btn-light"><?php echo $sortname?></button>
                </form>
            </th>
            <th scope="col" class="task-sort">
                <form action="handling.php">
                    <button type="submit" formmethod="post" name="<?php echo $sortemailbutton?>" class="btn btn-light"><?php echo $sortemail?></button>
                </form>
            </th>
        </tr>
        </thead>
        <tbody>
        <?php
        $sorttask = '';
        if(isset($_COOKIE['TaskSortDone'])) {
            if($_COOKIE['TaskSortDone'] == "A")
                $sorttask = 'done ASC';
            elseif ($_COOKIE['TaskSortDone'] == "Z")
                $sorttask = 'done DESC';
        } elseif(isset($_COOKIE['TaskSortName'])) {
            if($_COOKIE['TaskSortName'] == "A")
                $sorttask = 'name ASC';
            elseif ($_COOKIE['TaskSortName'] == "Z")
                $sorttask = 'name DESC';
        } elseif (isset($_COOKIE['TaskSortEmail'])) {
            if($_COOKIE['TaskSortEmail'] == "A")
                $sorttask = 'email ASC';
            elseif ($_COOKIE['TaskSortEmail'] == "Z")
                $sorttask = 'email DESC';
        };

        $counttask = $pdosql->query("SELECT count(*) FROM task")->fetchColumn();
        $pagetask = 1;
        if(isset($_GET['PageTask'])) {
            $pagetask = intval($_GET['PageTask']);
        }
        $pagetasksql = $pagetask * 3 - 3;
        $sqltask = $pdosql->query("SELECT * FROM task ORDER BY $sorttask LIMIT $pagetasksql , 3");
        foreach ($sqltask as $row) {
            ?>
            <tr class='task-list_task'>
            <?php if($row['Edit'] == 1) {
                ?>
                    <td class="task_edit"><span>отредактировано администратором</span></td>
                <?php
            } else {
                ?>
                    <td class="task_edit"></td>
                <?php
            }
            if($row['Done'] == 1) {
                ?>
              <td class="task_done"><div>&#10003;</div>
              <?php
                if(isset($_SESSION['admin_mode']) && $_SESSION['admin_mode'] === 1) {
              ?>
                <form class="edit-task" action="handling.php" method="post">
                  <input type="hidden" name="undonetask">
                  <input type="hidden" name="taskid" value="<?php echo $row['TaskID']?>">
                  <button type="submit" class="btn btn-primary">Undone</button>
                </form>
              </td>
              <?php
                }
            } else {
                ?>
              <td class="task_done"><div>X</div>
                <?php
                if(isset($_SESSION['admin_mode']) && $_SESSION['admin_mode'] === 1) {
                ?>
                <form class="edit-task" action="handling.php" method="post">
                  <input type="hidden" name="donetask">
                  <input type="hidden" name="taskid" value="<?php echo $row['TaskID'] ?>">
                  <button type="submit" class="btn btn-primary">Done</button>
                </form>
                <?php
                }
                ?>
              </td>
                <?php
            }?>
            <td><?php echo $row['Name']?></td>
            <td><?php echo $row['Email']?></td>
            <?php
                  if(isset($_SESSION['admin_mode']) && $_SESSION['admin_mode'] === 1) {
                    ?>
                    <td class="task-text_edit">
                        <p><?php echo $row['Text']?></p>
                        <form class="edit-task" action="handling.php" method="post" style="display: none">
                          <textarea class="form-control" name="edittexttask" cols="100" rows="5"><?php echo $row['Text'];?></textarea>
                          <input type="hidden" name="taskid" value="<?php echo $row['TaskID']?>">
                          <button type="submit" class="btn btn-primary">Edit</button>
                        </form>
                    </td>
                    <?php
                  } else {
                      echo "<td><p>".$row['Text']."</p></td>";
                  }
                ?>
           </tr>
            <?php
        }
        ?>
        </tbody>
    </table>

     <nav>
         <ul class="pagination justify-content-center">
             <?php
             if($counttask > 3) {
             $countpagetask = ceil($counttask/3);
             ?>
             <li class="page-item <?php if($pagetask==1) echo 'disabled'?>">
                 <a class="page-link" href="?PageTask=<?php echo $pagetask-1?>">Previous</a>
             </li>
             <?php
             for($i=1; $i <= $countpagetask; $i++) {
                 $pagetaskactive = '';
                 if($pagetask == $i)
                     $pagetaskactive = 'active';
                 echo "<li class='page-item $pagetaskactive'><a class='page-link' href='?PageTask=$i'>$i</a></li>";
             }
             ?>
             <li class="page-item <?php if($pagetask==$countpagetask) echo 'disabled'?>">
                 <a class="page-link" href="?PageTask=<?php echo $pagetask+1?>">Next</a>
             </li>
             <?php
             }
             ?>
         </ul>
     </nav>

    <form class="form-group create-task needs-validation" novalidate action="handling.php" method="post">
        <div class="form-row justify-content-md-center">
            <div class="form-group col-md-3">
                <label for="create-task_name">Name</label>
                <input type="text" name="Name" class="form-control <?php if(isset($_COOKIE['ErrName'])) echo 'is-invalid'?>" id="create-task_name" value="<?php if(isset($_COOKIE['TaskName'])) echo $_COOKIE['TaskName']?>" required>
                <div class="invalid-feedback">
                    Enter name!
                </div>
            </div>
            <div class="form-group col-md-3">
                <label for="create-task_email">Email</label>
                <input type="email" name="Email" class="form-control <?php if(isset($_COOKIE['ErrEmail'])) echo 'is-invalid'?>" id="create-task_email" value="<?php if(isset($_COOKIE['TaskEmail'])) echo $_COOKIE['TaskEmail']?>" required>
                <div class="invalid-feedback">
                    Enter valid Email!
                </div>
            </div>
        </div>
        <div class="form-row justify-content-md-center">
            <div class="col-md-6">
                <label for="create-task_text">Text Task</label>
                <textarea name="Text" class="form-control <?php if(isset($_COOKIE['ErrText'])) echo 'is-invalid'?>" id="create-task_text" required><?php if(isset($_COOKIE['TaskText'])) echo $_COOKIE['TaskText']?></textarea>
                <div class="invalid-feedback">
                    Enter text!
                </div>
            </div>
        </div>
        <button type="submit" class="btn btn-primary" name="createtask">Create Task</button>
    </form>

</section>