<?php
    session_start();
    include ('db.php');
?>


<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/html">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>test bj</title>
        <link rel="stylesheet" href="css/bootstrap.css">
        <link rel="stylesheet" href="js/bootstrap.js">
        <link rel="stylesheet" href="css/main.css">
        <script src="js/jquery-3.4.1.min.js"></script>
        <script src="js/main.js"></script>
    </head>

    <body>
    <?php
    if(isset($_COOKIE['ErrLoginAdmin']) && $_COOKIE['ErrLoginAdmin'] == 1){
        ?>
          <script>alert("Need authorization!")</script>
        <?php
    }
    if(isset($_COOKIE['SuccessAddTask']) && $_COOKIE['SuccessAddTask'] == 1){
        ?>
          <script>alert("Success add task!")</script>
        <?php
    }
    ?>
        <section class="reg-auth container d-flex justify-content-end">
            <?php
              if(isset($_SESSION['admin_mode']) && $_SESSION['admin_mode'] === 1) {
                ?>
                <form class="form-group reg-auth_form" action="handling.php" method="post">
                  <div class="input-group-prepend">
                    <div class="input-group-text">Admin</div>
                  </div>
                  <button type="submit" name="adminexit" class="btn btn-primary">Exit</button>
                </form>
              <?php
              } else {
                ?>
                <form class="form-group reg-auth_form" action="handling.php" method="post">

                  <input class="form-control <?php if(isset($_COOKIE['ErrAdmin']) || isset($_COOKIE['EmptyAdmin'])) echo 'is-invalid'?>" type="text" name="login" placeholder="Login">
                  <input class="form-control <?php if(isset($_COOKIE['ErrAdmin']) || isset($_COOKIE['EmptyAdmin'])) echo 'is-invalid'?>" type="password" name="password" placeholder="Password">
                  <div class="invalid-feedback">
                      <?php if(isset($_COOKIE['ErrAdmin'])) echo 'Invalid Login or Password!'?>
                      <?php if(isset($_COOKIE['EmptyAdmin'])) echo 'Need feel form'?>
                  </div>
                  <button type="submit" name="adminlogin" class="btn btn-primary">Login</button>
                </form>

                <?php
              }
            ?>
        </section>

    <?php
    include ('task-list.php');
    ?>

    </body>
</html>